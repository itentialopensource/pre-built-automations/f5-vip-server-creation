<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# F5 VIP Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview

This Pre-Built uses the [Ansible Big IP Virtual Server](https://docs.ansible.com/ansible/latest/modules/bigip_virtual_server_module.html) module to configure an F5 BigIP Load Balancer to create VIP Servers and apply appropriate profiles, iRules and security settings during the process.

## Operations Manager and JSON-Form

This workflow has an [Operations Manager Item](./bundles/ac_agenda_jobs/F5%20VIP%20Creation.json) that calls a workflow. The Operations Manager Item uses a JSON-Form to specify common fields populated when an issue is created. The workflow the Operations Manager item calls queries data from the formData job variable.

<table>
  <tr>
    <td>
      <img src="./images/f5_vip_mgmt_ac_form.png" alt="form" width="800px">
    </td>
  </tr>
  <tr>
    <td>
      <img src="./images/f5_vip_mgmt_canvas.png" alt="form" width="800px">
    </td>
  </tr>
</table>


## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2023.1`
* Itential Automation Gateway
  * `^3.227.0+2023.1.15`

## Requirements

This Pre-Built requires the following:

* F5 BigIP Load Balancer on-boarded to IAG

## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows Zero Touch operation
* Notifications place holder

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Item `F5 VIP Creation` or call [F5 VIP Management](./bundles/workflows/F5%20VIP%20Management.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "destination": "Destination IP of the virtual server",
  "device": "Target F5 host",
  "name": "Virtual server name",
  "poolName": "Default pool for the virtual server",
  "port": "Port of the virtual server (required when state is present and the virtual server does not exist)"
  "state": "The virtual server state (present, absent, enabled or disabled)",
}
```


## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
